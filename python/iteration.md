## Smart methods of iteration in python

There are quite a few ways to iterate through a list or a dictionary in python:

```py
# iterate through the indices and then refer to the item
for x in range(len(data)):
    print(data[x])
    ...

# or fetch an item directly
for x in data:
    print(x)
    ...
```

### Enumerate

There's an option to do both of these at the same time. Fetch `i` - index, and `x` - item

```py
for i, x in enumerate(data):
    print(data[i])
    print(x)
```

This code prints each item twice.

### Dictionaries

Suppose we have a dictionary:

```py
dict = {'a': 1, 'b': 2, 'c': 3}
```

Then this is how you iterate through:
1. The keys:
```py
for key in dict.keys():
    print(key)
```
2. The values:
```py
for value in dict.values():
    print(value)
```
3. Both keys and values:
```py
for key, value in dict.items():
    print(f'{key}: {value}')
```