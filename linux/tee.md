## Typical `tee` use case scenarios

You will mostly use `tee` within a bash pipe to print both to `stdout` and another file.

```bash
echo "Hello World!" | tee "hello.txt"
```

Running this program will print `Hello World!` and at the same time write it as a content to `hello.txt`.

Checking it with `cat hello.txt` yields:
```
Hello World!
```