## Shell script

Generally you will use shell scripts to solve simple system-related things.

```bash
#!/bin/bash
```

How to create a simple bash script:

```bash
touch script.sh # create a new file called script.sh
... # edit the script using vim, nano, or any other editor
chmod +x script.sh # inform the system that the script is an executable
./script.sh # execute the script
```

### Bash `$` versatile operator

A dollar `$` operator does quite a few things in bash.
1. It refers to a previously declared variable
```bash
str="hello"
echo "${str}"
```
2. It allows command substitution:
```bash
files=$(ls | wc -l) # list-storage [pipe] word count (-l) lines
```
3. It is used as a positional parameter (script arguments)
```bash
./script.sh 1 hello
... # inside script.sh
echo $0 # yields script.sh
echo $1 # yields 1
echo $2 # yields hello
```
4. It is used to evaluate arithmetic expressions
```bash
a=5
b=10
res=$((a+b)) # keep the double brackets
echo "${res}"
```
5. Refer to the environment variables
```bash
echo "${PATH}"
```

### Conditional clauses

Clause operators
- `eq` (equal)
- `ne` (not equal)
- `lt` (less than)
- `le` (less than or equal),
- `gt` (greater than)
- `ge` (greater than or equal)