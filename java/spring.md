### Spring

Don't use gradle, trust me.

```java
@SpringBootApplication
@RestController
public class MavenSimpleBogApplication {

    public static void main(String[] args) {
        SpringApplication.run(MavenSimpleBogApplication.class, args);
    }

    @GetMapping(path="/",produces= MediaType.APPLICATION_JSON_VALUE)
    public String home() {
        return "{\"message\":\"Hello World\"}";
    }

}
```
